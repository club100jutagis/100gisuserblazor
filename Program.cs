using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MatBlazor;
using Blazored.LocalStorage;

using _100GisUser.Library;

namespace _100GisUser
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            var baseUrl = "https://devel.club100jutagis.com/api/";
            // var baseUrl = "https://club100jutagis.com/api/";

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(baseUrl) });

            // MatBlazor
            builder.Services.AddMatBlazor();
            builder.Services.AddMatToaster(config =>
            {
                config.Position = MatToastPosition.BottomLeft;
                config.NewestOnTop = false;
                config.MaximumOpacity = 100;
                config.ShowCloseButton = false;
                config.PreventDuplicates = true;
                config.VisibleStateDuration = 3000;
                config.ShowTransitionDuration = 100;
                config.MaxDisplayedToasts = 10;
                config.HideTransitionDuration = 100;
            });

            // Blazored.LocalStorage
            builder.Services.AddBlazoredLocalStorage();

            // Own Services
            builder.Services.AddScoped<ISecureLocalStorage, SecureLocalStorage>();
            builder.Services.AddScoped<IStateService, StateService>();

            await builder.Build().RunAsync();
        }
    }
}
