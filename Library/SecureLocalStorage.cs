using System;
using Blazored.LocalStorage;
using Newtonsoft.Json;
using System.Text;
using Microsoft.JSInterop;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _100GisUser.Library
{

    public interface ISecureLocalStorage
    {
        public Task<T> GetItemAsync<T>(string keyName);
        public Task SetItemAsync(string keyName, Object item);
        public ValueTask RemoveItemAsync(string keyName);
        public ValueTask ClearAsync();
        public ValueTask<bool> ContainKeyAsync(string keyName);

    }

    public class SecureLocalStorage : ISecureLocalStorage
    {

        private ILocalStorageService _localStorage;
        private IJSRuntime _js;

        private readonly int[] key = {144, 45, 88, 40, 87, 249, 125, 162, 78, 246, 245, 88, 109, 251, 105, 24};
        private readonly int[] iv = {3, 85, 254, 156, 29, 238, 26, 155, 120, 98, 200, 205, 202, 104, 230, 182};

        public SecureLocalStorage(ILocalStorageService localStorage, IJSRuntime JS)
        {
            _localStorage = localStorage;
            _js = JS;
        }

        public async Task<T> GetItemAsync<T>(string keyName)
        {

            var str64 = await _localStorage.GetItemAsync<string>(keyName);
            var cipherBytes = Convert.FromBase64String(str64);

            // Decrypt
            int[] cipherInts = cipherBytes.Select(x => (int)x).ToArray();
            byte[] strBytesDecypt = (await _js.InvokeAsync<IList<byte>>("AesDecrypt", key, iv, cipherInts)).ToArray();

            char[] strChars = new char[Encoding.UTF8.GetCharCount(strBytesDecypt, 0, strBytesDecypt.Length)];
            Encoding.UTF8.GetChars(strBytesDecypt, 0, strBytesDecypt.Length, strChars, 0);

            // JSON object in string
            string json = new String(strChars);

            // JSON Deserialize
            var item = JsonConvert.DeserializeObject<T>(json);
            return item;
        }

        public async Task SetItemAsync(string keyName, Object item)
        {

            // JSON Serialize
            var json = JsonConvert.SerializeObject(item);
            byte[] jsonBytes = Encoding.UTF8.GetBytes(json);

            // encrypt
            int[] jsonInts = jsonBytes.Select(x => (int)x).ToArray();
            byte[] cipher = (await _js.InvokeAsync<IList<byte>>("AesEncrypt", key, iv, jsonInts)).ToArray();

            // Convert into base64 string
            var str64 = Convert.ToBase64String(cipher);

            await _localStorage.SetItemAsync<string>(keyName, str64);
        }

        public ValueTask RemoveItemAsync(string keyName)
        {
            return _localStorage.RemoveItemAsync(keyName);
        }

        public ValueTask ClearAsync()
        {
            return _localStorage.ClearAsync();
        }

        public ValueTask<bool> ContainKeyAsync(string keyName)
        {
            return _localStorage.ContainKeyAsync(keyName);
        }

    }

}