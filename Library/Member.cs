using System;

namespace _100GisUser.Library
{

    public class Member
    {
        public int IdMember { get; set; }
        public int KodeMember { get; set; }
        public string NamaMember { get; set; }
    }
}