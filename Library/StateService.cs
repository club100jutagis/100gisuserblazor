using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using System.Net.Http;
using System.Net.Http.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace _100GisUser.Library
{

    public interface IStateService
    {

        // Functions
        public Task<bool> IsLoginAsync();
        public Task<bool> RouteDefault();
        public Task<bool> LogoutAsync();
        public Task<bool> SetLastPageAsync(string pageName);
        public Task<MemberAuth> GetCurrentUserAsync();

    }

    public class StateService : IStateService
    {

        private ISecureLocalStorage _secureStorage;
        private NavigationManager _navMgr;
        private HttpClient _http;

        /**
         * PROPERTIES
         */

        /**
         * CONSTRUCTOR
         */
        public StateService(ISecureLocalStorage secureStorage, NavigationManager navigationManager, HttpClient httpClient)
        {
            _secureStorage = secureStorage;
            _navMgr = navigationManager;
            _http = httpClient;

        }

        /**
         * FUNCTIONS
         */
        #region FUNCTIONS
        public async Task<bool> IsLoginAsync()
        {
            var isVarSet = await _secureStorage.ContainKeyAsync("var");

            // not in login state
            if(!isVarSet)
            {
                _navMgr.NavigateTo("login");
                return false;
            }
            return true;
        }

        public async Task<bool> RouteDefault()
        {

            // Not in login state
            if(!(await IsLoginAsync()))
            {
                return false;
            }

            // lastpage undefined in localstorage
            if(!(await _secureStorage.ContainKeyAsync("last")))
            {
                _navMgr.NavigateTo("dashboard");
                return true;
            }

            var lastPage = await _secureStorage.GetItemAsync<string>("last");
            _navMgr.NavigateTo(lastPage);
            return true;
        }

        public async Task<bool> LogoutAsync()
        {
            await _secureStorage.RemoveItemAsync("var");
            _navMgr.NavigateTo("login");

            return true;
        }

        public async Task<bool> SetLastPageAsync(string pageName)
        {
            await _secureStorage.SetItemAsync("last", pageName);

            return true;
        }

        public async Task<MemberAuth>GetCurrentUserAsync()
        {
            if(!(await _secureStorage.ContainKeyAsync("var")))
            {
                return null;
            }

            return await _secureStorage.GetItemAsync<MemberAuth>("var");
        }

        #endregion // FUNCTIONS

    }
}