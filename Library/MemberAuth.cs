using System;

namespace _100GisUser.Library
{

    public class MemberAuth
    {
        public int? IdMember { get; set; }
        public int? KodeMember { get; set; }
        public string NamaMember { get; set; }
        public int? IdLevelMember { get; set; }
        public string NamaLevelMember { get; set; }
        public bool? PunyaAlat { get; set; }
        public Token Token { get; set; }

    }

}
