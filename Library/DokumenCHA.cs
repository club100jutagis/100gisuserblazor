using System;

namespace _100GisUser.Library
{
    public class DokumenCHA{

        public DateTime TglPengujian { get; set; }
        public DateTime TglUpload { get; set; }
        public string FileCha { get; set; }
        public string KodeMember { get; set; }
        public string NamaMember { get; set; }

    }
}

