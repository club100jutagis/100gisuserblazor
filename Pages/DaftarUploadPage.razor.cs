using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using moment.net;
using Microsoft.JSInterop;
using System.Net.Http;
using System.Net.Http.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MatBlazor;
using System.Linq;

using _100GisUser.Library;

namespace _100GisUser.Pages
{

    public partial class DaftarUploadPage
    {

        private IList<DokumenCHA> _dokumenCHAs = new List<DokumenCHA>();
        private DokumenCHA[] sortedCHA= null;

        /**
         * HANDLERS
         */
        #region HANDLER

        // Blazor Component OnInitialized
        protected override async Task OnInitializedAsync()
        {
            await Initialize();
        }

         // Open new tab to file CHA URL
        private async Task showDokumenCHAHandler(object item)
        {
            var dokumenCha = item as DokumenCHA;
            await _JS.InvokeVoidAsync("open", dokumenCha.FileCha, "_blank");
        }

        // Table Sort Handler
        private void SortHandler(MatSortChangedEvent sort)
        {
            sortedCHA = _dokumenCHAs.ToArray();
            if(!(sort == null || sort.Direction == MatSortDirection.None || string.IsNullOrEmpty(sort.SortId)))
            {
                Comparison<DokumenCHA> comparison = null;
                switch(sort.SortId)
                {
                    case "tglUpload":
                        comparison = (s1, s2) => s1.TglUpload.CompareTo(s2.TglUpload);
                        break;
                    case "namaMember":
                        comparison = (s1, s2) => string.Compare(s1.NamaMember, s2.NamaMember, StringComparison.InvariantCultureIgnoreCase);
                        break;
                    case "tglPengujian":
                        comparison = (s1, s2) => s1.TglPengujian.CompareTo(s2.TglPengujian);
                        break;
                }
                if(comparison != null)
                {
                    if(sort.Direction == MatSortDirection.Desc)
                    {
                        Array.Sort(sortedCHA, (s1, s2) => -1 * comparison(s1, s2));
                    }
                    else
                    {
                        Array.Sort(sortedCHA, comparison);
                    }
                }
            }
        }

        #endregion
        // HANDLERS Region


        /**
         * FUNCTIONS
         */

        #region FUNCTIONS

        // Initialize
        private async Task<bool> Initialize()
        {
            // check login state
            if(!(await _state.IsLoginAsync()))
            {
                return false;
            }

            // Set lastpage
            await _state.SetLastPageAsync("daftarupload");

            // Get AUTH TOKEN
            var token = (await _state.GetCurrentUserAsync()).Token.LoadData;

            // fetch data from backend
            var page = 1;
            var finished = false;

            // fetch data till it's finished
            while(!finished)
            {
                // Console.WriteLine($"Fetch page : {page}");
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("token", token),
                    new KeyValuePair<string, string>("page", Convert.ToString(page))
                });

                var res = await _http.PostAsync("Dokumen_cha/load_data_upload", content);


                var jobj = JObject.Parse(await res.Content.ReadAsStringAsync());

                // problem from backend
                if(!(bool)jobj["sts"])
                {
                    // Token problem
                    if((string)jobj["message"] == "token_bermasalah")
                    {
                        await _state.LogoutAsync();
                        return false;
                    }

                    _toaster.Add((string)jobj["message"], MatToastType.Danger, "Kesalahan", "error_outline");
                    return false;
                }

                JArray data = jobj["data"].ToObject<JArray>();

                // breaking loop
                if(data.Count <= 0)
                {
                    finished = true;
                }

                // Storing data
                for(var i=0;i<data.Count;i++)
                {
                    var fileUri = (string)data[i]["file_cha"];
                    _dokumenCHAs.Add(new DokumenCHA
                    {
                        FileCha = $"https://club100jutagis.com/{fileUri}",
                        TglPengujian = data[i]["tgl_pengujian"].ToObject<DateTime>(),
                        TglUpload = data[i]["tgl_upload"].ToObject<DateTime>(),
                        KodeMember = (string)data[i]["kode_member"],
                        NamaMember = (string)data[i]["nama_member"],
                    });
                }

                // increment page
                page++;
            } // While

            // Initiate sort and showing the data
            SortHandler(null);

            return true;
        }

        #endregion
        // FUNCTIONS Region

    }
}