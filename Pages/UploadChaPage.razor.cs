using System.Threading.Tasks;
using MatBlazor;
using System.IO;
using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.JSInterop;
using Microsoft.AspNetCore.Components.Web;
using System.Net.Http;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using moment.net;
using System.Text;

using _100GisUser.Library;


namespace _100GisUser.Pages
{
    public partial class UploadChaPage{

        // Services DI
        private string _selectedNamaMember = "";
        private Member _selectedMember = null;
        private DateTime _tglPengujian = DateTime.Now;

        // File Upload
        private MatFileUpload _filePicker;
        private bool _ready = false;

        private IMatFileUploadEntry _chaFile = null;

        // Cari Member
        private MatDialog _cariMemberDialog;
        private string _cariMemberText = "";
        private IList<Member> _membersCari = new List<Member>();
        private Member[] _sortedMembersCari = {};

        /**
         * HANDLERS
         */
        #region HANDLERS

        // Blazor Component OnInitialized
        protected override async Task OnInitializedAsync()
        {
            var token = (await _state.GetCurrentUserAsync()).Token;
            // Console.WriteLine(token.LoadData);
            // Console.WriteLine(token.SendData);

            await Initialize();
        }

        // show cari member dialog
        private void showCariMemberDialogHandler()
        {
            // Show Cari Member dialog
            _cariMemberDialog.IsOpen = true;
        }

        // KeyUP "enter" button handler to cariMember
        private async Task cariMemberHandler(KeyboardEventArgs e)
        {
            if(e.Code == "Enter"){
                await fetchDataMemberCari();
            }
        }

        // pilih Member dari member dialog table
        private void pilihMemberHandler(object item)
        {
            var member = item as Member;
            _selectedMember = member;
            _selectedNamaMember = $"{member.KodeMember} - {member.NamaMember}";
            _cariMemberDialog.IsOpen = false;
            _cariMemberText = "";

            if(validasiInput()){
                _ready = true;
            }
        }

        // File Upload OnChanged Handler
        private async Task<bool> matFileUploadOnChanged(IMatFileUploadEntry[] files)
        {

            // file not PDF
            if(files[0].Type != "application/pdf")
            {
                await _dialogService.AlertAsync("File bukan PDF");

                // Change the STATE
                if(_chaFile != null)
                {
                    _ready = true;
                }
                else
                {
                    _ready = false;
                }

                // Exit Function
                return false;
            }

            _chaFile = files[0];

            var ms = new MemoryStream();

            await files[0].WriteToStreamAsync(ms);

            var hasil = ms.ToArray().Select(x => (int)x).ToArray();
            await _JS.InvokeVoidAsync("viewPDFInViewer", hasil);

            // Change the STATE
            if(validasiInput()){
                _ready = true;
            }

            return true;
        }

        // Upload Button CLick
        private async Task uploadChaHandler()
        {
            var result = await _dialogService.AskAsync("Data yang diinput sudah benar?", new string[] {"Ya", "Belum"} );

            if(result == "Ya")
            {
                // Upload FORM DATA
                await uploadCha();
            }
        }

        // CANCEL BUTTON Click
        private void cancelHandler()
        {
            cleanupInputs();
        }

        private void SortHandler(MatSortChangedEvent sort)
        {
            _sortedMembersCari = _membersCari.ToArray();
            if(!(sort == null || sort.Direction == MatSortDirection.None || string.IsNullOrEmpty(sort.SortId)))
            {
                Comparison<Member> comparison = null;
                switch(sort.SortId)
                {
                    case "kodeMember":
                        comparison = (s1, s2) => s1.KodeMember.CompareTo(s2.KodeMember);
                        break;
                    case "namaMember":
                        comparison = (s1, s2) => string.Compare(s1.NamaMember, s2.NamaMember, StringComparison.InvariantCultureIgnoreCase);
                        break;
                }
                if(comparison != null)
                {
                    if(sort.Direction == MatSortDirection.Desc)
                    {
                        Array.Sort(_sortedMembersCari, (s1, s2) => -1 * comparison(s1, s2));
                    }
                    else
                    {
                        Array.Sort(_sortedMembersCari, comparison);
                    }
                }
            }
        }

        #endregion
        // HANDLERS REGION


        /**
         * FUNCTIONS
         */
        #region FUNCTIONS

        // INITIALIZE
        private async Task<bool> Initialize()
        {
            // check login state
            if(!(await _state.IsLoginAsync()))
            {
                return false;
            }

            // Set lastpage
            await _state.SetLastPageAsync("uploadcha");

            return true;
        }

        // VALIDATE all INPUTS
        private bool validasiInput(){
            if(_selectedMember == null){
                return false;
            }

            if(_chaFile == null){
                return false;
            }

            return true;
        }

        // CLEAN up all INPUTS
        private void cleanupInputs(){
            _ready = false;
            _chaFile = null;
            _selectedMember = null;
            _selectedNamaMember = "";
            _cariMemberText = "";
            _tglPengujian = DateTime.Now;
        }

        // Fetch data from server with keyword in _cariMemberText
        private async Task<bool> fetchDataMemberCari()
        {
            // Get AUTH TOKEN
            var token = (await _state.GetCurrentUserAsync()).Token;

            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("token", token.LoadData),
                new KeyValuePair<string, string>("child", "false"),
                new KeyValuePair<string, string>("with_me", "false"),
                new KeyValuePair<string, string>("jml_data", "200"),
                new KeyValuePair<string, string>("keyword_member", _cariMemberText),
                new KeyValuePair<string, string>("page", "1")
            });

            var res = await _http.PostAsync("member/load_member_simple", content);

            if(!res.IsSuccessStatusCode)
            {
                _toaster.Add("Ada ganguan pada Server / Jaringan", MatToastType.Danger, "Kesalahan", "error_outline");
                return false;
            }

            var jobj = JObject.Parse(await res.Content.ReadAsStringAsync());

            // problem from backend
            if(!(bool)jobj["sts"])
            {
                // Token problem
                if((string)jobj["message"] == "token_bermasalah")
                {
                    await _state.LogoutAsync();
                    return false;
                }

                _toaster.Add((string)jobj["message"], MatToastType.Danger, "Kesalahan", "error_outline");
                return false;
            }

            var membersCari = new List<Member>();
            JArray data = jobj["data"].ToObject<JArray>();

            // Storing data
            for(var i=0;i<data.Count;i++)
            {
                var fileUri = (string)data[i]["file_cha"];
                membersCari.Add(new Member
                {
                    IdMember = (int)data[i]["id_member"],
                    KodeMember = (int)data[i]["kode_member"],
                    NamaMember = (string)data[i]["nama_member"]
                });
            }

            _membersCari = membersCari;

            // Initiate sort and showing the data
            SortHandler(null);

            return true;
        } // fetchDataMemberCari

        private async Task<bool> uploadCha()
        {
            var form = new MultipartFormDataContent();

            var ms = new MemoryStream();
            await _chaFile.WriteToStreamAsync(ms);
            var fileContent = new ByteArrayContent(ms.ToArray());
            fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");

            var tglPengujianStr = _tglPengujian.Format("yyyy-MM-dd");
            form.Add(fileContent, "file_cha", $"{_selectedMember.KodeMember}-{_selectedMember.NamaMember}-{tglPengujianStr}-cha.pdf");

            // token Content
            var token = (await _state.GetCurrentUserAsync()).Token;
            form.Add(new StringContent((await _state.GetCurrentUserAsync()).Token.SendData), "token");
            form.Add(new StringContent(Convert.ToString((await _state.GetCurrentUserAsync()).IdMember)), "id_member_alat");
            form.Add(new StringContent(Convert.ToString(_selectedMember.IdMember)), "id_member");
            form.Add(new StringContent(tglPengujianStr), "tgl_pengujian");

            var res = await _http.PostAsync("dokumen_cha/simpan", form);

            if(!res.IsSuccessStatusCode)
            {
                _toaster.Add("Ada ganguan pada Server / Jaringan", MatToastType.Danger, "Kesalahan", "error_outline");
                return false;
            }

            var konten = await res.Content.ReadAsStringAsync();
            Console.WriteLine(konten);
            var jobj = JObject.Parse(konten);

            // problem from backend
            if(!(bool)jobj["sts"])
            {
                _toaster.Add((string)jobj["data"], MatToastType.Danger, "Kesalahan", "error_outline");
                Console.WriteLine(konten);
                return false;
            }

            _toaster.Add((string)jobj["data"], MatToastType.Success, "Sukses", "error_outline");

            // CLEARING INPUTS
            cleanupInputs();

            return true;

        } // uploadCha

        #endregion
        // FUNCTIONS REGION

    }
}