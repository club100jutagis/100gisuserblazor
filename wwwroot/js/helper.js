
// PDF VIEWER
window.viewPDFInViewer = function(pdfData){

    // Check Viewer Elemet
    var viewerEl = document.querySelector(`#pdfviewer`);

    // No Viewer Element
    if(!viewerEl){
        console.log(`Viewer Element doesn't exist`);
        return;
    }

    // Using DocumentInitParameters object to load binary data.
    var loadingTask = pdfjsLib.getDocument({data: pdfData});
    loadingTask.promise.then(function(pdf) {
        console.log('PDF loaded');

        // Fetch the first page
        var pageNumber = 1;
        pdf.getPage(pageNumber).then(function(page) {
            viewerEl.style.display = "block";

            var scale = 1.5;
            var viewport = page.getViewport({scale: scale});

            // Prepare canvas using PDF page dimensions
            var canvas = viewerEl;
            var context = canvas.getContext('2d');
            canvas.height = viewport.height;
            canvas.width = viewport.width;

            // Render PDF page into canvas context
            var renderContext = {
                canvasContext: context,
                viewport: viewport
            };
            var renderTask = page.render(renderContext);
            renderTask.promise.then(function () {
                console.log('Page rendered');
            });
        });
    }, function (reason) {
        // PDF loading error
        console.error(reason);
    });
}

/**
 * Crypto
 */
let aesCryptKey;
window.AesEncrypt = async function(key, counter, dataraw){

    key = Uint8Array.from(key);
    counter = Uint8Array.from(counter);
    dataraw = Uint8Array.from(dataraw);

    var aesKey = await crypto.subtle.importKey("raw", key.buffer, 'AES-CTR', false, ["encrypt"]);
    const dataCipher = await window.crypto.subtle.encrypt(
    {
        name: "AES-CTR",
        counter: counter,
        length: 64
    }, aesKey, dataraw);

    var cipherArr = new Uint8Array(dataCipher);
    return Array.from(cipherArr);
}

window.AesDecrypt = async function(key, counter, datacipher){

    key = Uint8Array.from(key);
    counter = Uint8Array.from(counter);
    datacipher = Uint8Array.from(datacipher);

    var aesKey = await crypto.subtle.importKey("raw", key.buffer, 'AES-CTR', false, ["decrypt"]);
    var rawData = await window.crypto.subtle.decrypt(
    {
        name: "AES-CTR",
        counter: counter,
        length: 64
    }, aesKey, datacipher);

    var rawArr = new Uint8Array(rawData);
    return Array.from(rawArr);
}